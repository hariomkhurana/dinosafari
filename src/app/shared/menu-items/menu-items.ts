import { Injectable } from "@angular/core";

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [
  { state: "video-management", type: "link", name: "Intro Video", icon: "theaters" },
  { state: "audio-management", type: "link", name: "Audio Tour", icon: "settings_voice" },
  { state: "setting-management", type: "link", name: "Settings", icon: "settings" }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
