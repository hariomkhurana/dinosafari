import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../shared/common.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['../full.component.scss']
})
export class AppHeaderComponent implements OnInit{
  userData:any;
  constructor(private commonService: CommonService) { }
  
  ngOnInit(
    
  ) {
    this.userData = this.commonService.getUser();
  }

  logoutUser() { 
      this.commonService.logOut();
  }



}

