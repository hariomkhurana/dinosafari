import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MainRoutingModule } from "./main-routing.module";
import { MainComponent } from "./main.component";
import { FullComponent } from "../layouts/full/full.component";
import { AppSidebarComponent } from "../layouts/full/sidebar/sidebar.component";
import { AppHeaderComponent } from "../layouts/full/header/header.component";
import { SharedModule } from "../shared/shared.module";
import { AudioManagementModule } from "./audio-management/audio-management.module";
import { VideoManagementModule } from "./video-management/video-management.module";
import { SettingManagementModule } from "./setting-management/setting-management.module";
@NgModule({
  declarations: [MainComponent, FullComponent, AppSidebarComponent, AppHeaderComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    AudioManagementModule,
    VideoManagementModule,
    SettingManagementModule,
    SharedModule,

    // DemoMaterialModule
  ]
})
export class MainModule {}
