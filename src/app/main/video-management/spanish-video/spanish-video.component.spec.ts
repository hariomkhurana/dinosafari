import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpanishVideoComponent } from './spanish-video.component';

describe('SpanishVideoComponent', () => {
  let component: SpanishVideoComponent;
  let fixture: ComponentFixture<SpanishVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpanishVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpanishVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
