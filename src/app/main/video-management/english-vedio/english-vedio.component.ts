import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModalOptions, NgbModalRef, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material';
import Swal from 'sweetalert2';
import { CommonService } from '../../../shared/common.service';
import { VedioService } from '../vedio.service';

@Component({
  selector: 'app-english-vedio',
  templateUrl: './english-vedio.component.html',
  styleUrls: ['./english-vedio.component.css']
})
export class EnglishVedioComponent implements OnInit {
  data:any;
  userDetails;
  rowDetail: any;
  descriptionDetails: any;
  btntext = "button"
  query: string;
  statusFormSubmitted: boolean = false;
  statusForm: FormGroup;
  statusList: Array<object> = [
    { id: "active", name: "Active" },
    { id: "inactive", name: "Inactive" },
    { id: "blocked", name: "Block" }
  ];

  // END - Update user variable
  // filters
  searchStartDate: string;
  searchEndDate: string;
  searchUserStatus: string = "all";
  searchBy: string = "all";
  searchText: string;
  // filters end

  List = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 10;
  userSearchType: number = 0;
  minDate: any;
  maxDate: any;
  isLoading: boolean = false;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;
  userToken: any
  headTitle: string = "Add Video";
  label: string = "";
  selectedFile: File;
  constructor(private MyvedioService: VedioService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private commonService: CommonService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getVediolistDetail();
    this.initialForm();
  }


  showVedio: boolean = false;
  getVediolistDetail() {
    this.isLoading = true;
    this.MyvedioService.getData("").subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.List = [];
          this.List.push(res["data"]['video']);
          this.showVedio = true;
        } else {
          this.List = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  openwebView(value) {
    window.open(value, '_blank');
  }
  uploadAttachment(event) {

    this.selectedFile = event.target.files[0];
    this.label = this.selectedFile.name;
    this.isLoading = true;
    this.MyvedioService.uploadVedio(this.selectedFile,this.label).subscribe(res => {
      this.isLoading = false;
      if (res["message"] == "Success") { 
      
        this.isLoading = false;
        this.statusForm.controls['url'].setValue(res.data.url);
        this.selectedFile = null;
      } else {
         
        
      }
    }, err => {
     this.isLoading = false
    });
  }

  initialForm() {
    this.statusForm = this.formBuilder.group({
      url: ["", [Validators.required]],
      title: ["", [Validators.required]]
    });
  }
  patchValue(data) {
    this.statusForm.patchValue({
      title: data['title'],
      url: data['url']
    });
    let length = data['url'].length;
    this.label = data['url'];
    this.label = this.label.substring(length - 40, length)
  }
  updateUserStatusModal(row, content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: 'md'
    };
    if (row !== 'row') {
      this.headTitle = 'Edit Video';
      this.patchValue(row);
    }
    this.data = row;
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();

    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  updateUserStatus(){
    if(this.statusForm.valid) {
      this.isLoading = true;
      let req = this.statusForm.value;
      req['videoId'] = this.data['videoId'];
      this.MyvedioService.updateVidio(req).subscribe(
        res => {
          this.isLoading = false;
          if ((res["message"] = "Success")) {
            this.modalRef.close();
            this._snackBar.open("Video updated successfully!", "", {
              duration: 5000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: ['success']
            });
            this.getVediolistDetail();
          } else {
            this.List = [];
          }
          this.isLoading = false;
        },
        err => {
          this.isLoading = false;
        }
      );
    }
  }


  // deleteModal(row) {
  //   Swal.fire({
  //     title: 'Are you sure?',
  //     text: "you want to delete this video",
  //     icon: 'warning',
  //     showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: 'Yes, delete it!'
  //   }).then((result) => {
  //     if (result.isConfirmed) {
  //       this.isLoading = true;
  //       this.MyvedioService.DeleteVedio(row.videoId).subscribe(
  //         res => {
  //           this.isLoading = false;
  //           if (res["message"] == "Success") {
  //             this._snackBar.open("video deleted successfully!", "", {
  //               duration: 5000,
  //               horizontalPosition: 'right',
  //               verticalPosition: 'top',
  //               panelClass: ['success']
  //             });
  //             // Swal.fire("Success", "Officer updated successfully!", "success");
  //             this.getVediolistDetail();
  //           } else {
  //           }
  //         },
  //         err => {
  //           this.isLoading = false;
  //         }
  //       );
  //     }
  //   })
  // }


  swalImage(image) {
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,
      imageHeight: 400,
    })
  }
  // END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

}
