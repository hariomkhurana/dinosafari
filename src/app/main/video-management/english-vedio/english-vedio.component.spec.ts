import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnglishVedioComponent } from './english-vedio.component';

describe('EnglishVedioComponent', () => {
  let component: EnglishVedioComponent;
  let fixture: ComponentFixture<EnglishVedioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnglishVedioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnglishVedioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
