import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { VideoManagementRoutingModule } from "./video-management-routing.module";
import { VideoManagementComponent } from "./video-management.component";
import { ViewVideoComponent } from "./view-video/view-video.component";
import { SharedModule } from "../../shared/shared.module";
import { EnglishVedioComponent } from './english-vedio/english-vedio.component';
import { SpanishVideoComponent } from './spanish-video/spanish-video.component';
import { VedioService } from "./vedio.service";
@NgModule({
  declarations: [VideoManagementComponent, ViewVideoComponent, EnglishVedioComponent, SpanishVideoComponent],
  imports: [CommonModule, VideoManagementRoutingModule, SharedModule],
  providers: [VedioService]
})
export class VideoManagementModule {}
