import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { VideoManagementComponent } from "../video-management/video-management.component";
import { EnglishVedioComponent } from "./english-vedio/english-vedio.component";
import { SpanishVideoComponent } from "./spanish-video/spanish-video.component";

const routes: Routes = [
  {
    path: "",
    component: VideoManagementComponent,
    children: [
      { path: "", redirectTo: "/main/video-management/view-english", pathMatch: "full" },
      { path: "view-spanish", component: SpanishVideoComponent },
      { path: "view-english", component: EnglishVedioComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideoManagementRoutingModule {}
