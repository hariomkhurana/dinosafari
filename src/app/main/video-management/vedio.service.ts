import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NetworkService } from '../../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class VedioService {
  public httpOptions = {};
  constructor(private _networkService: NetworkService) { }


  getData(videoId) {
    return this._networkService.get(
      "api/data?" + videoId,
      null,
      null,
      "bearer"
    );
  }
  updateVidio(body) {
    return this._networkService.put(
      "api/videos",
      body,
      null,
      "bearer"
    );
  }

  
  uploadVedio(image: any,name:any) {
    const formData = new FormData();

    formData.append("file", image,name);
    return this._networkService.uploadImages("api/s3upload/media-upload", formData, null, "bearer");
  }

}
