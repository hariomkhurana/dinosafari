import { Component, OnInit } from '@angular/core'; import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModalOptions, NgbModalRef, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material';
import Swal from 'sweetalert2';
import { CommonService } from '../../../shared/common.service';
import { AudioMgmtService } from '../audio.service';

@Component({
  selector: 'app-view-english',
  templateUrl: './view-english.component.html',
  styleUrls: ['./view-english.component.css']
})
export class ViewEnglishComponent implements OnInit {
  data;
  rowDetail: any;
  descriptionDetails: any;
  btntext = "button"
  query: string;
  statusFormSubmitted: boolean = false;
  statusForm: FormGroup;
  statusList: Array<object> = [
    { id: "active", name: "Active" },
    { id: "inactive", name: "Inactive" },
    { id: "blocked", name: "Block" }
  ];

  // END - Update user variable
  // filters
  searchStartDate: string;
  searchEndDate: string;
  searchUserStatus: string = "all";
  searchBy: string = "all";
  searchText: string;
  // filters end

  List = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 20;
  userSearchType: number = 0;
  minDate: any;
  maxDate: any;
  addForm:FormGroup;
  isLoading: boolean = false;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;
  userToken: any
  headTitle: string = "Add Spanish Audio";
  label: string = "";
  selectedFile: File;
  constructor(private MyvedioService: AudioMgmtService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private commonService: CommonService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getVediolistDetail();
    this.initialForm();
  }


  showVedio: boolean = false;
  getVediolistDetail() {
    this.isLoading = true;
    this.MyvedioService.getData("").subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.List = res["data"]['audioList'];

          console.log(this.List)
        } else {
          this.List = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  openwebView(value) {
    window.open(value, '_blank');
  }

  initialForm() {
    this.statusForm = this.formBuilder.group({
      url: ["", [Validators.required]],
      title: ["", [Validators.required]]
    });

    this.addForm = this.formBuilder.group({
      spanishTitle: ["", [Validators.required]],
      englishTitle: ["", [Validators.required]],
      spanishUrl: ["", [Validators.required]],
      englishUrl: ["", [Validators.required]],
      englishDuration: ["", [Validators.required]],
      spanishDuration: ["", [Validators.required]],
    });
  }
  patchValue(data) {
    this.statusForm.patchValue({
      title: data['spanishTitle'],
      url: data['spanishUrl']
    });
    let length = data['spanishUrl'].length;
    this.label = data['spanishUrl'];
    this.label = this.label.substring(length - 40, length)
  }
  updateUserStatusModal(row, content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: 'md'
    };
    if (row !== 'row') {
      this.headTitle = 'Edit Spanish Audio';
      this.patchValue(row);
    }
    this.data = row;
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();

    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  updateUserStatusModal1(row, content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size: 'md'
    };
    this.headTitle = 'Add Audio';
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();

    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  uploadAttachment(event,type) {

    // console.log('event cAlling',event)
   if(type =='ENGLISH'){
    this.isLoading = true;
    this.selectedFile = event.target.files[0];
    this.label = this.selectedFile.name;

    this.getVedioDurationEnglish();
    this.MyvedioService.uploadAudio(this.selectedFile,this.label).subscribe(res => {
      this.isLoading = false;
      if (res["message"] == "Success") { 
      
        this.isLoading = false;
        this.addForm.controls['englishUrl'].setValue(res.data.url);
        this.selectedFile = null;
      } else {
         
        
      }
    }, err => {
     this.isLoading = false
    });
   }else{
    this.isLoading = true;
    this.selectedFile = event.target.files[0];
    this.label = this.selectedFile.name;
   console.log('selected FIle',this.selectedFile)
   this.getVedioDurationSpanish();
    this.MyvedioService.uploadAudio(this.selectedFile,this.label).subscribe(res => {
      this.isLoading = false;
      if (res["message"] == "Success") { 
      
        this.isLoading = false;
        this.addForm.controls['spanishUrl'].setValue(res.data.url);
        this.selectedFile = null;
      } else {
         
        
      }
    }, err => {
     this.isLoading = false
    });
   }
  }
  uploadAttachment1(event) {
    if(this.statusForm.valid) {
      this.selectedFile = event.target.files[0];
      this.label = this.selectedFile.name;
      this.getVedioDurationEdit();
      this.MyvedioService.uploadAudio(this.selectedFile,this.label).subscribe(res => {
        this.isLoading = false;
        if (res["message"] == "Success") { 
        
          this.isLoading = false;
          this.statusForm.controls['url'].setValue(res.data.url);
          this.selectedFile = null;
        } else {
           
          
        }
      }, err => {
       this.isLoading = false
      });
    }
  }
  duration:any;
  getVedioDurationEnglish() {
      new Audio(URL.createObjectURL(this.selectedFile)).onloadedmetadata = (e:any) =>{
          this.duration = Math.round(e.currentTarget.duration);
          this.addForm.controls['englishDuration'].setValue(this.duration);
      }
  
    }
    getVedioDurationSpanish() {
      new Audio(URL.createObjectURL(this.selectedFile)).onloadedmetadata = (e:any) =>{
          this.duration = Math.round(e.currentTarget.duration);
          this.addForm.controls['spanishDuration'].setValue(this.duration);
      }
  
    } 
    getVedioDurationEdit() {
      new Audio(URL.createObjectURL(this.selectedFile)).onloadedmetadata = (e:any) =>{
          this.duration = Math.round(e.currentTarget.duration);
      }
  
    }  
  updateUserStatus(){
    if(this.statusForm.valid) {

     if(this.headTitle =='Add Spanish Audio') {
      let req ={
        englishUrl:this.statusForm.value.url,
        englishTitle:this.statusForm.value.tittle,
        duration:this.duration,
        audioId	:this.data['audioId'],

      }
      this.MyvedioService.updateAudio(req).subscribe(
        res => {
          this.isLoading = false;
          if ((res["message"] = "Success")) {
            this._snackBar.open("Audio updated successfully!", "", {
              duration: 5000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: ['success']
            });
            this.getVediolistDetail();
          } else {
            this.List = [];
          }
          this.isLoading = false;
        },
        err => {
          this.isLoading = false;
        }
      );
     }else 
     {
      let req ={
        spanishUrl:this.statusForm.value.url,
        spanishTitle:this.statusForm.value.title,
        audioId	:this.data['audioId'],
        spanishDuration:this.duration,
      }
      console.log('final Req,',req)
      this.MyvedioService.updateAudio(req).subscribe(
        res => {
          this.isLoading = false;
          if ((res["message"] = "Success")) {
            this.getVediolistDetail();
            
  
            console.log(this.List)
          } else {
            this.List = [];
          }
          this.isLoading = false;
        },
        err => {
          this.isLoading = false;
        }
      );
     }
    }
  }

  SubmitAudio(){
    if(this.addForm.valid) {
      this.isLoading = true;
      let req = this.addForm.value;
      this.MyvedioService.AddAudio(req).subscribe(
        res => {
          this.isLoading = false;
          if ((res["message"] = "Success")) {
            this.modalRef.close();
            this._snackBar.open("Audio added successfully!", "", {
              duration: 5000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
              panelClass: ['success']
            });
            this.getVediolistDetail();
  
            console.log(this.List)
          } else {
            this.List = [];
          }
          this.isLoading = false;
        },
        err => {
          this.isLoading = false;
        }
      );
    }
  }

  deleteModal(row) {
    Swal.fire({
      title: 'Are you sure?',
      text: "you want to delete this audio",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.isLoading = true;
        let req ={
          status:'deleted',
          audioId:row.audioId,
        }
        this.MyvedioService.updateAudio(req).subscribe(
          res => {
            this.isLoading = false;
            if (res["message"] == "Success") {
              this._snackBar.open("Audio deleted successfully!", "", {
                duration: 5000,
                horizontalPosition: 'right',
                verticalPosition: 'top',
                panelClass: ['success']
              });
              // Swal.fire("Success", "Officer updated successfully!", "success");
              this.getVediolistDetail();
            } else {
            }
          },
          err => {
            this.isLoading = false;
          }
        );
      }
    })
  }


  swalImage(image) {
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,
      imageHeight: 400,
    })
  }
  // END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

}


