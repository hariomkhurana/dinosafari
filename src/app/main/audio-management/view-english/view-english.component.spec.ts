import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEnglishComponent } from './view-english.component';

describe('ViewEnglishComponent', () => {
  let component: ViewEnglishComponent;
  let fixture: ComponentFixture<ViewEnglishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewEnglishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEnglishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
