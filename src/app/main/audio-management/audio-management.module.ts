import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AudioManagementRoutingModule } from "./audio-management-routing.module";
import { AudioManagementComponent } from "./audio-management.component";
import { ViewAudioComponent } from "./view-audio/view-audio.component";
import { SharedModule } from "../../shared/shared.module";
import { AudioMgmtService } from "./audio.service";
import { ViewEnglishComponent } from "./view-english/view-english.component";
import { ViewSpanishComponent } from "./view-spanish/view-spanish.component";
@NgModule({
  declarations: [AudioManagementComponent, ViewAudioComponent, ViewSpanishComponent, ViewEnglishComponent],
  imports: [CommonModule, AudioManagementRoutingModule, SharedModule],
  providers: [AudioMgmtService]
})
export class AudioManagementModule {}
