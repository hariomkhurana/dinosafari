import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AudioManagementComponent } from "../audio-management/audio-management.component";
import { ViewAudioComponent } from "./view-audio/view-audio.component";
import { ViewSpanishComponent } from "./view-spanish/view-spanish.component";
import { ViewEnglishComponent } from "./view-english/view-english.component";

const routes: Routes = [
  {
    path: "",
    component: AudioManagementComponent,
    children: [
      { path: "", redirectTo: "/main/audio-management/view-english", pathMatch: "full" },
      { path: "view-audio", component: ViewAudioComponent },
      { path: "view-spanish", component: ViewSpanishComponent },
      { path: "view-english", component: ViewEnglishComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AudioManagementRoutingModule {}
