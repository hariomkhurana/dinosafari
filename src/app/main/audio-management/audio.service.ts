import { Injectable } from '@angular/core';
import { NetworkService } from '../../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class AudioMgmtService {
  public httpOptions = {};
  constructor(private _networkService: NetworkService) { }


  getData(videoId) {
    return this._networkService.get(
      "api/data?" + videoId,
      null,
      null,
      "bearer"
    );
  }
  AddAudio(body) {
    return this._networkService.post(
      "api/audios",
      body,
      null,
      "bearer"
    );
  }
  updateAudio(body) {
    return this._networkService.put(
      "api/audios",
      body,
      null,
      "bearer"
    );
  }

  DeleteAudio(videoId) {
    return this._networkService.delete(
      "api/audios/" + videoId,
      null,
      null,
      "bearer"
    );
  }

  
  uploadAudio(image: any,name:any) {
    const formData = new FormData();

    formData.append("file", image,name);
    return this._networkService.uploadImages("api/s3upload/media-upload", formData, null, "bearer");
  }

}
