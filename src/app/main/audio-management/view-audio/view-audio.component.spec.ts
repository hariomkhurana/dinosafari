import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAudioComponent } from './view-audio.component';

describe('ViewAudioComponent', () => {
  let component: ViewAudioComponent;
  let fixture: ComponentFixture<ViewAudioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAudioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
