import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSpanishComponent } from './view-spanish.component';

describe('ViewSpanishComponent', () => {
  let component: ViewSpanishComponent;
  let fixture: ComponentFixture<ViewSpanishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSpanishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSpanishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
