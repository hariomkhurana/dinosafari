import { Injectable } from "@angular/core";
import { NetworkService } from "../../shared/network.service";


@Injectable()
export class SettingMgmtService {
  constructor(private networkService: NetworkService) {}

  getChildProfileApi(query) {
    return this.networkService.get(
      "api/child/?" + query,
      null,
      null,
      "bearer"
    );
  }

  postimageApi(body: any) {
    return this.networkService.post("api/webview", body, null, "bearer");
  }

  getDocumentApi() {
    return this.networkService.get("api/webview", null, null, "bearer");
  }
  uploadImage(image: any) {
    const formData = new FormData();

    formData.append("file", image);
    return this.networkService.uploadImages("api/s3upload/media-upload", formData, null, "bearer");
  }
 

  
  
  

  
}
