import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingManagementRoutingModule } from './setting-management-routing.module';
import { SettingManagementComponent } from './setting-management.component';
import { ViewSettingComponent } from './view-setting/view-setting.component';
import { AccountComponent } from './account/account.component';
import { PasswordComponent } from './password/password.component';

import { SettingMgmtService } from './setting.service';
import { SharedModule } from '../../shared/shared.module';
import { FileService } from '../../shared/fileService';


@NgModule({
  declarations: [SettingManagementComponent, ViewSettingComponent,AccountComponent,PasswordComponent],
  imports: [
    CommonModule,
    SettingManagementRoutingModule,
    SharedModule
  ],
  providers:[FileService,SettingMgmtService]
})
export class SettingManagementModule { }
