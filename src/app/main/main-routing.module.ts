import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MainComponent } from "./main.component";
import { FullComponent } from "../layouts/full/full.component";
// import { DashboardComponent } from './dashboard/dashboard.component';

import { AuthGuardMain } from "../auth/auth-guard.service";
//import { FutureBookingComponent } from './booking-management/future-booking/future-booking.component';

const routes: Routes = [
  // { path: "", redirectTo: "/main/dashboard", pathMatch: "full" },
  {
    path: "main",
    component: MainComponent,
    canActivate: [AuthGuardMain],
    children: [
      { path: "", redirectTo: "/main/video-management", pathMatch: "full" },
      {
        path: "audio-management",
        loadChildren: () =>
          import("../main/audio-management/audio-management.module").then(m => m.AudioManagementModule)
      },
      {
        path: "video-management",
        loadChildren: () =>
          import("../main/video-management/video-management.module").then(m => m.VideoManagementModule)
      },
      {
        path: "setting-management",
        loadChildren: () =>
          import("../main/setting-management/setting-management.module").then(m => m.SettingManagementModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {}
